using System;
using Xunit; //for the testing
using Calculator;

namespace CalculatorTest
{
    public class CalculatorTest
    {
        [Fact] //For testing single outcomes (Data Attribute)

        //Provide proper name for the test
        // <Method> + <scenario> + <expected behvaior>
        public void Add_AddTwoNumber_ShouldReturnSum() 
        {
            /* AAA */

            //Arrange = Create
            //Create an Object
            StandardCalculator calculator = new StandardCalculator();

            int lhs = 1;
            int rhs = 1;
            int expected = lhs + rhs;

            //Act = Peform
            int actual = calculator.Add(lhs, rhs);

            //Assert = Confirm
            Assert.Equal(expected, actual);

            //Crate another test with Theory
            
        }
        [Theory]
        [InlineData(1, 1, 2)] //pass different test datas
        [InlineData(-1, 1, 0)]

        public void Add_AddTwoNumber_ShouldReturnSum_Theory(int lhs, int rhs, int expected)
        {
            //Arrange
            StandardCalculator calculator = new StandardCalculator();

            //Act
            int actual = calculator.Add(lhs, rhs);

            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Divide_TwoNumbers_ReturnDivisionOfNumbers()
        {
           //Arrange
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 2;
            int rhs = 1;
            int expected = lhs / rhs;

            //Act = Peform
            int actual = calculator.Divide(lhs, rhs);

            //Assert = Confirm
            Assert.Equal(expected, actual);            

        }
        //Value 1 divided by 0 - 
        [Fact]
        public void Divide_WithZero_ShouldThrowDivideByZeroException()
        {
            //Arrange
            StandardCalculator calculator = new StandardCalculator();
            int lhs = 1;
            int rhs = 0;

            //Act + Assert //Anonymous function
            Assert.Throws<DivideByZeroException> (() => calculator.Divide(lhs, rhs));                  
        }
    }
}
