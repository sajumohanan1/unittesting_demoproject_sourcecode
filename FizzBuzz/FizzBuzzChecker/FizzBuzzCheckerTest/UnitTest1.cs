using FizzBuzzChecker;
using System;
using Xunit;

namespace FizzBuzzCheckerTest
{
    public class UnitTest1
    {
        [Fact]
        public void CheckFizzBuzz_NumberIsMultipleOfThreeOnly_Fuzz()
        {
            //Arrange
            FuzzBuzzChecker checker = new FuzzBuzzChecker();
            int num = 3;
            string expected = "Fizz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CheckFizzBuzz_NumberIsMultipleOfFiveOnly_Buzz()
        {
            //Arrange
            FuzzBuzzChecker checker = new FuzzBuzzChecker();
            int num = 5;
            string expected = "Buzz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }


        [Theory]
        [InlineData(3)]
        [InlineData(6)]
        [InlineData(9)]
        public void CheckFizzBuzz_NumberIsMultileOf3and5_FizzBuzz(int num)
        {
            //Arrange
            FuzzBuzzChecker checker = new FuzzBuzzChecker();            
            string expected = "FizzBuzz";

            //Act
            string actual = checker.CheckFizzBuzz(num);

            //Assert
            Assert.Equal(expected, actual);
        }

        //Exception handling - if you pass the negative number
        [Fact]
        public void CheckFizzBuzz_NumberIsNegative_ThrowsInvalidNumberException()
        {
            //Arrange
            FuzzBuzzChecker checker = new FuzzBuzzChecker();
            int num = -1;

            //Act & Assert
            Assert.Throws<Exception>(() => checker.CheckFizzBuzz(num));
        }

       }
}
